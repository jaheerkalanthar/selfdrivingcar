import cv2
#Our Image
imgFile="Images/carDataSet.png"
#clasifierFile= #HaarCascasde Algorithm

#Train the algorithm Car Classifier

Harrcascade="cars.xml"
image=cv2.imread(imgFile)

#convert image into a grey,Because it is help us to find the images easily
greyishImage=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
#cvtColor means Convert Color
#In common every Color has RGB that means pixels
#But in grey there is only two varient (white and Black)

#It classify somthing
classifier=cv2.CascadeClassifier(Harrcascade)

#Detect CArs in given image
detectCars=classifier.detectMultiScale(greyishImage)
#Get Pixels

print(detectCars)

#It will show the image file


#Image Drawn in rectangle
for i in range(len(detectCars)):#For find all image file
    x,y,h,w=detectCars[i]#Unpack list
    detectCarsImage= cv2.rectangle(image,(x,y),(x+w,y+h),(0,0,255),3)


cv2.imshow("Car Image",detectCarsImage)
#It will show the image until we press the key
key=cv2.waitKey()

print("Hello world")